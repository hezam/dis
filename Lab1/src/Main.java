import java.io.IOException;

public class Main {

    public static void main(String[] args) {
        long start = System.currentTimeMillis();

       if(args.length < 1){
           System.out.println("501 - Invalid Number of Arguments\n");
           return;
       }
        if(!Methods.ifSize()){
            System.out.println("Wrong 501");;
        }

        Thread t1 = new Thread() {
            public synchronized void run() {
                try {
                    Methods.readFileRUBEUR(args[0]);
                    System.out.println(Methods.Calc());

                } catch (IOException e) {
                  //  e.printStackTrace();
                    System.err.println("Error:502 cont find file 1");

                }
                return;
            }
        };

        Thread t2 = new Thread() {
            public synchronized void run() {
                try {
                    Methods.readFileRUBUSD(args[1]);
                    Methods.Calc();
                } catch (IOException e) {
                   // e.printStackTrace();
                    System.err.println("Error:502 cont find file 2");

                }
                return;
            }
        };
        t1.start();
       t2.start();

        try {
            t1.join();
            t2.join();
        } catch (InterruptedException e1) {
            e1.printStackTrace();
        }

        long end = System.currentTimeMillis();
        if (args.length > 2 && args[2].equals("-t")) {
            System.out.println("Time :" + (end - start) + "ms");
        }

    }


}