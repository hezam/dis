package org.hezamMaven;

public class Driver {

    private int IdDriver;
    private String name;
    private int number;

    public Driver() {
    }

    public Driver(int idDriver, String name, int number) {
        IdDriver = idDriver;
        this.name = name;
        this.number = number;
    }

    public int getIdDriver() {
        return IdDriver;
    }

    public void setIdDriver(int idDriver) {
        IdDriver = idDriver;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
