public class Format {

    // TICKER; PER; DATE; TIME; CLOSE

    String ticker;
    int per;
    int date;
    int time;
    double close;

    public Format(String ticker, int per, int date, double close) {
        this.ticker = ticker;
        this.per = per;
        this.date = date;
        this.close = close;
    }

    public String getTicker() {
        return ticker;
    }

    public int getPer() {
        return per;
    }

    public int getDate() {
        return date;
    }

    public int getTime() {
        return time;
    }

    public double getClose() {
        return close;
    }

    @Override
    public String toString() {
        return
                "ticker='" + ticker + '\'' +
                        ", per=" + per +
                        ", data=" + date +
                        ", time=" + time +
                        ", close=" + close +
                        '}' + "\n";
    }
}

