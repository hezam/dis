import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;

public class Methods {

    private static final List<Format> FormatListRUBEUR = new LinkedList<>();
    private static final List<Format> FormatListRUBUSD = new LinkedList<>();


    public static void readFileRUBEUR(String path) throws IOException {
        BufferedReader br;
        FormatListRUBEUR.sort(new Comparator<Format>() {
            @Override
            public int compare(Format o1, Format o2) {
                return o1.date - o2.date;
            }
        });

        br = new BufferedReader(new FileReader(path));
        br.readLine();
        String line;
        while ((line = br.readLine()) != null) {
            String[] data;
            data = line.split(",");
            FormatListRUBEUR.add(new Format(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]),
                    Double.parseDouble(data[4])));
        }
    }
    public static void readFileRUBUSD(String path) throws IOException {
        BufferedReader br;
        FormatListRUBUSD.sort(new Comparator<Format>() {
            @Override
            public int compare(Format o1, Format o2) {
                return o1.date - o2.date;
            }
        });
        br = new BufferedReader(new FileReader(path));
        br.readLine();
        String line;
        while ((line = br.readLine()) != null) {
            String[] data;
            data = line.split(",");
            FormatListRUBUSD.add(new Format(data[0], Integer.parseInt(data[1]), Integer.parseInt(data[2]),
                    Double.parseDouble(data[4])));
        }
    }

    public static boolean ifSize() {
        if (FormatListRUBUSD.size() == FormatListRUBEUR.size()) {
            return true;
        }

        return false;
    }

    static int sizeList1 = 0;
    static int sizeList2 = 0;

    public static double AverageX() {

        double x = 0;
        for (Format format : FormatListRUBEUR) {
            x += format.close;
        }
        return x / FormatListRUBEUR.size();
    }
    public static double AverageY() {
        double y = 0;
        for (Format format : FormatListRUBUSD) {
            y += format.close;
        }
        return y / FormatListRUBUSD.size();
    }

    public static double[] XAverage_x() {
        int i = 0;
        sizeList1 = FormatListRUBEUR.size();
        double[] doubles = new double[sizeList1];
        double X_x = 0;
        for (Format format : FormatListRUBEUR) {
            doubles[i++] = X_x += format.close - AverageX();
        }
        return doubles;
    }
    public static double[] YAverage_y() {
        double Y_y = 0;
        sizeList2 = FormatListRUBUSD.size();
        int i = 0;
        double[] doubles = new double[sizeList2];
        for (Format format : FormatListRUBUSD) {
            doubles[i++] = Y_y += format.close - AverageY();
        }
        return doubles;
    }

    public static double powX(){
        double sum = 0;
        double[] arr = XAverage_x();
        for (int i = 0; i <arr.length; i++) {
             sum += Math.pow(arr[i], 2);
        }
        return Math.sqrt(sum);
    }
    public static double powY(){
        double sum = 0;
        double[] arr = YAverage_y();
        for (int i = 0; i <arr.length; i++) {
            sum += Math.pow(arr[i] , 2);
        }
        return Math.sqrt(sum);
    }

    public static double maulArr(double[] arr1 , double[] arr2) {
        double result = 0;
        for (int i = 0; i < arr1.length; i++) {
            double num1 = arr1[i];
            double num2 = arr2[i];
            result += (num1 * num2);
        }
        return result;

    }

    public static double Calc(){

        return  maulArr(XAverage_x(),YAverage_y()) / (powX()* powY());
    }

    public static void print_list()
    {
        System.out.println(Arrays.toString(FormatListRUBEUR.toArray()));
        System.exit(1);
    }
}
