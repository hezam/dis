-- 1. Найти полеты (flights) из аэропорта Казани в Краснодар


SELECT * FROM 
	flights fli WHERE fli.departure_airport 
			= (SELECT airport_code FROM airports_data WHERE city ->> 'ru' = 'Казань') 
	AND fli.arrival_airport 
			= (SELECT airport_code FROM airports_data WHERE city ->> 'ru' = 'Краснодар');




    --  2 Найти все полеты из Москвы (все аэропорты) за 25 минут (за какую дату и час придумайте сами)
    	
select * from 
	flights fli where 
		(SELECT city ->> 'ru' FROM airports_data where airport_code = fli.departure_airport )='Москва' 
	and
	actual_departure between '2017-07-30 12:30:00+03' and (	timestamp '2019-03-30 02:50:00+03' + 25 * interval '1 minutes'); 
	
	
	-- 3. Показать все полеты в одной временной зоне (полеты и аэропорты отобразить вместе)

select f.* from 
airports_data a join flights f on a.airport_code  = f.departure_airport and a.timezone= 'Europe/Moscow';


