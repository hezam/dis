package org.hezamMaven;

public class Client {

    private int idClint;
    private String name;
    private String email;
    private int number;

    public Client() {
    }

    public Client(int idClint, String name, String email, int number) {
        this.idClint = idClint;
        this.name = name;
        this.email = email;
        this.number = number;
    }

    public int getIdClint() {
        return idClint;
    }

    public void setIdClint(int idClint) {
        this.idClint = idClint;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public int getNumber() {
        return number;
    }

    public void setNumber(int number) {
        this.number = number;
    }
}
