package org.hezamMaven;

import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.*;
import java.util.LinkedList;
import java.util.List;

public class Main {
    public static void main(String[] args) {

//        Client client1 = new Client(1, "ali", "ali@gmail.com", 991532135);
//
//        Driver driver1 = new Driver(1, "Ahmed", 951532651);
//
//        Car car1 = new Car(123, "Mosteng", "Blue");
//
//        Order order1 = new Order(client1, driver1, car1, "22-09-2021");

        ObjectMapper objectMapper = new ObjectMapper();


        List<Car> cars = new LinkedList<>();
        cars.add(new Car(1, "Mosteng", "Blue"));
        cars.add(new Car(2, "PMW", "red"));
        cars.add(new Car(3, "KAI", "black"));

        List<Client> clients = new LinkedList<>();
        clients.add(new Client(1, "ali", "ali@gmail.com", 991532135));
        clients.add(new Client(2, "mohammed", "mohammed@gmail.com", 94552443));
        clients.add(new Client(3, "ahmed", "ahmed@gmail.com", 943252335));

        List<Driver> drivers = new LinkedList<>();
        drivers.add(new Driver(1, "amer", 951532651));
        drivers.add(new Driver(2, "sami", 951234654));
        drivers.add(new Driver(3, "jamal", 951345245));

        List<Order> orders = new LinkedList<>();
        orders.add(new Order(getClientById(1 , clients) , getDriverById(1 , drivers) , getCarById( 1 , cars) , "1-08-2021"));
        orders.add(new Order(getClientById(2 , clients) , getDriverById(2 , drivers) , getCarById( 2 , cars) , "15-08-2021"));
        orders.add(new Order(getClientById(3 , clients) , getDriverById(3 , drivers) , getCarById( 3 , cars) , "19-08-2021"));





        try {

            objectMapper.writeValue(new File("client.json"), orders);

        } catch (IOException e) {

            e.printStackTrace();
        }
    }
    private static Car getCarById(int idCar, List<Car> cars) {
        for (Car car : cars) {
            if (idCar == car.getIdCar())
                return car;
        }
        return null;
    }
    private static Client getClientById(int idClient, List<Client> clients) {
        for (Client client : clients) {
            if (idClient == client.getIdClint())
                return client;
        }
        return null;
    }
    private static Driver getDriverById(int idDriver , List<Driver> drivers){
        for(Driver driver : drivers){
            if (idDriver == driver.getIdDriver())
                return driver;
        }
        return null;
    }
}
