package org.hezamMaven;

public class Car {
    private int idCar;
    private String TypeCar;
    private String carColor;

    public Car() {
    }

    public Car(int idCar, String typeCar, String carColor) {
        this.idCar = idCar;
        TypeCar = typeCar;
        this.carColor = carColor;
    }

    public int getIdCar() {
        return idCar;
    }

    public void setIdCar(int idCar) {
        this.idCar = idCar;
    }

    public String getTypeCar() {
        return TypeCar;
    }

    public void setTypeCar(String typeCar) {
        TypeCar = typeCar;
    }

    public String getCarColor() {
        return carColor;
    }

    public void setCarColor(String carColor) {
        this.carColor = carColor;
    }
}
