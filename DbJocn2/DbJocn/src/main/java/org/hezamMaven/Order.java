package org.hezamMaven;

public class Order {
    private Client client;
    private Driver driver;
    private Car car;
    private String data;

    public Order() {
    }

    public Order(Client client, Driver driver, Car car, String data) {
        this.client = client;
        this.driver = driver;
        this.car = car;
        this.data = data;
    }


    public Client getClient() {
        return client;
    }

    public void setClient(Client client) {
        this.client = client;
    }

    public Driver getDriver() {
        return driver;
    }

    public void setDriver(Driver driver) {
        this.driver = driver;
    }

    public Car getCar() {
        return car;
    }

    public void setCar(Car car) {
        this.car = car;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
