package dis.lab2.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;

public class Server {

    private static Socket clientSocket;
    private static ServerSocket server;
    private static BufferedReader in;
    private static BufferedWriter out;

    static int randNum = new Random().nextInt(10);
    public static void main(String[] args) {

        try {
            try {
                server = new ServerSocket(10000);
                System.out.println("Сервер запущен!");
                clientSocket = server.accept();
                System.out.println("Клиент подключился!");
                try {
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));

                    System.out.println(randNum +"\n");
                    for (int i = 0; i < 3; i++) {
                        String word = in.readLine();
                        System.out.println(word);
                        if (word.equals(String.valueOf(randNum))) {
                            out.write("Matches"+"\n");
                            out.flush();
                            break;
                        } else {
                            out.write("Doesn't Match"+"\n");
                        }
                        out.flush();
                    }
                } finally {
                    clientSocket.close();
                    in.close();
                    out.close();
                }
            } finally {
                System.out.println("Сервер закрыт!");
                server.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }
    }
}