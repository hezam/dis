package dis.lab2.client;

import java.io.*;
import java.net.Socket;

public class Client {

    private static Socket clientSocket;
    private static BufferedReader reader;
    private static BufferedReader in;
    private static BufferedWriter out;

    public static void main(String[] args) {
        try {
            try {

                clientSocket = new Socket("127.0.0.1", 10000);
                reader = new BufferedReader(new InputStreamReader(System.in));
                in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                out = new BufferedWriter(new OutputStreamWriter(clientSocket.getOutputStream()));


                System.out.println("Угадай число от одного до десяти:");


                for (int i = 0; i <3 ; i++) {
                    String word = reader.readLine();
                    out.write( word+ "\n");
                    out.flush();
                    String input = in.readLine();
                    if (input.equals("Matches")){
                        System.out.println("Is Equal ");
                        break;
                    }else {
                        System.out.println("Doesn't Equal"+"\n");
                        if (i < 2)
                        System.out.println("Try again");

                    }
                }
            } finally {
                System.out.println("Клиент был закрыт...");
                clientSocket.close();
                in.close();
                out.close();
            }
        } catch (IOException e) {
            System.err.println(e);
        }

    }
}