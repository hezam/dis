package ru.itis.lab3;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class Server {


    private static ServerSocket server;


    public static void main(String[] args) throws IOException {
             Map<String , List<Message>> queue = new HashMap<>();
             Map<String , Writer> clientStream = new HashMap<>();
            try {
                server = new ServerSocket(10000);
                System.out.println("server waiting connection!");

                while (true){
                    Socket clientSocket = server.accept();
                    System.out.println("client" + clientSocket.toString());
                    new ClientHandler(clientSocket,queue, clientStream).start();
                    //new ClientHandler(clientSocket,queue, clientStream).process();

                }

            }catch (IOException e){
                System.out.println(e);
            }finally {
                System.out.println("server has closed");
                try {
                    server.close();
                }catch (IOException e){
                    e.printStackTrace();
                }
            }
    }
}
